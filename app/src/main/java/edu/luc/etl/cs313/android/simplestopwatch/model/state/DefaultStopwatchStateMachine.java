package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultStopwatchStateMachine implements StopwatchStateMachine {

    public DefaultStopwatchStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private StopwatchState state;

    /*
    Method takes in a new instantiated state as a parameter and then the setter sets the state which then the uiUpdateListener
    takes in and updates the state of the app.
    For example in the RunningState class, there exists a method onTick()that sets the state of the machine to the countdown state
    (sm.toCountDownState();) after the pause time after last time clicking the button equals to 3 seconds (sm.actionGetPauseTime() == 3).
    When sm.toCountDownState(); is called, in this class the toCountDownState() method calls the function setState down below with parameter
    COUNTDOWN which is a new instance of the COUNTDOWN state being created.
     */
    protected void setState(final StopwatchState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private StopwatchUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final StopwatchUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onStartStop() { state.onStartStop(); }
    @Override public synchronized void onTick()      { state.onTick(); }
<<<<<<< Updated upstream
    //@Override public synchronized void onLapReset()  { state.onLapReset(); }
=======
   // @Override public synchronized void onPause()     {state.onPause();}
>>>>>>> Stashed changes

    @Override public void updateUIRuntime() { uiUpdateListener.updateTime(timeModel.getRuntime()); }

    // known states
    private final StopwatchState STOPPED     = new StoppedState(this);
    private final StopwatchState RUNNING     = new RunningState(this);
    private final StopwatchState COUNTDOWN   = new CountDownState(this);
    private final StopwatchState ALARMED     = new AlarmedState(this);


    // transitions

    @Override public void toRunningState()    { setState(RUNNING); }
    @Override public void toStoppedState()    { setState(STOPPED); }
    @Override public void toCountDownState()  { setState(COUNTDOWN); }
    @Override public void toAlarmedState()    { setState(ALARMED); }

    // actions

    /*
    These actions are called in the different state classes (such as StoppedState) in  methods such as
    "onStartStop()" or "onTick()".
    For example when the Start Stop button in the app is pressed when the machine is in a stopped state,
     the onStartStop() method calls state machine functions that creates actions accordingly. So when the button is pressed in the
     stopped state, "sm.actionStart(), sm.actionInc(), and sm.toRunningState()" all happen, starting the clock model, incrementing the time,
     and setting the state of the machine to the runningState as expected.
     */
    @Override public void actionInit()       { toStoppedState(); actionReset(); }
    @Override public void actionReset()      { timeModel.resetRuntime(); actionUpdateView(); }
    @Override public void actionStart()      { clockModel.start(); }
    @Override public void actionStop()       { clockModel.stop(); }
    @Override public void actionIncPauseTime() {timeModel.incPausetime();}
    @Override public int actionGetPauseTime()   { return timeModel.getPausetime(); }
    @Override public int actionGetRunTime()     { return timeModel.getRuntime(); }
    @Override public void actionResetPause() { timeModel.resetPausetime(); }
    @Override public void actionInc()        { timeModel.incRuntime(); actionUpdateView(); }
    @Override public void actionDec()        {timeModel.decRuntime(); actionUpdateView();}
    @Override public void actionUpdateView() { state.updateView(); }

    // @Override public void actionLap()        { timeModel.setLaptime(); }
}
