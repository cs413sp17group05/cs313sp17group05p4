package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class StoppedState implements StopwatchState {

    public StoppedState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStart(); //Starts the clock model
        sm.actionInc(); //Increment the runtime (the time visible)
        sm.toRunningState(); //Switches to the running state
    }

    /*@Override
    public void onLapReset() {
        sm.actionReset();
        sm.toStoppedState();
    }*/

    @Override
    public void onTick() { //No need for ticks in the stop state
        throw new UnsupportedOperationException("onTick");
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }
}
