package edu.luc.etl.cs313.android.simplestopwatch.common;

/**
 * A listener for stopwatch events coming from the UI.
 *
 * @author laufer
 */

/*
Listens to all of the events that come from pressing the button in the different states that occur (runningstate, stoppedstate, etc).
It is extended by the Stopwatchstate interface
 */

public interface StopwatchUIListener {
    void onStartStop();
   // void onLapReset();
    //no need for lapreset in a timer
    //deleted all the methods including lapping
}
