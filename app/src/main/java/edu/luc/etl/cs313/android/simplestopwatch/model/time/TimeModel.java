package edu.luc.etl.cs313.android.simplestopwatch.model.time;

/**
 * The passive data model of the stopwatch.
 * It does not emit any events.
 *
 * @author laufer
 */

public interface TimeModel {
    void resetRuntime();
    void incRuntime();
    void decRuntime(); //decrement the runtime
    int getRuntime();
    void resetPausetime(); //Used to reset the pause time everytime the increment button is clicked
    void incPausetime(); //Without any clicks on increment of runtime, pause time is increased
    int getPausetime(); //getter
    //void setPausetime(); //setter


    //void setLaptime();
    //int getLaptime();
}
