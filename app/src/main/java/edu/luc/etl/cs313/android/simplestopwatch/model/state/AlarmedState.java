package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import org.junit.rules.Stopwatch;
import android.media.AudioManager;//used for Stream_Notification which is "used to identify the volume of audio streams for notifications"
import android.media.ToneGenerator; //Used to provide the alert tone
import edu.luc.etl.cs313.android.simplestopwatch.R;


/**
 * Created by iband on 4/10/2017.
 */

public class AlarmedState implements StopwatchState{
    private final StopwatchSMStateView sm;
    public AlarmedState(final StopwatchSMStateView sm){this.sm = sm;}

    @Override
    public void onStartStop(){
        sm.actionStop(); //Stops the alarm
        sm.actionInit(); //Resets everything
    }

    @Override
    public void onTick(){ //Plays an alarming sound every tick
        final ToneGenerator alert = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
        alert.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
    }

    @Override
    public void updateView(){
        sm.updateUIRuntime(); //updates the runtime visual
    }

    @Override
    public int getId(){
        return R.string.ALARMING; //Brings up at the state ID at the top that it's alarming
    }
}
