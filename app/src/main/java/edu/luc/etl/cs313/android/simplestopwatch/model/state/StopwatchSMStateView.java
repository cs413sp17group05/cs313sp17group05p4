package edu.luc.etl.cs313.android.simplestopwatch.model.state;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer
 */


/*
All the different state changes and actions that will occur based on clicking the button and then depending on the state
 */
interface StopwatchSMStateView {

    // transitions
    void toRunningState();
    void toStoppedState();
    void toAlarmedState();
    void toCountDownState();
    //void toLapRunningState();
    //void toLapStoppedState();

    // actions
    void actionInit(); //Resets the clock and goes to stopped state
    void actionReset(); //Resets the runtime and view
    void actionStart(); //Starts the clock model
    void actionStop(); //Stops the clock model
    void actionInc(); //Increments the runtime
    void actionDec(); //Decrements the runtime
    int actionGetRunTime(); //Getter for runtime
    void actionUpdateView(); //Updates the view
    void actionResetPause(); //Resets the pause time
    void actionIncPauseTime(); //Increments the pause time
    int actionGetPauseTime(); //Getter for pause time
    //void actionLap();

    // state-dependent UI updates
    void updateUIRuntime();
    //void updateUILaptime();
}
