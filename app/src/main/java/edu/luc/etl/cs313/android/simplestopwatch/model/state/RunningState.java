package edu.luc.etl.cs313.android.simplestopwatch.model.state;
import android.media.AudioManager;//used for Stream_Notification which is "used to identify the volume of audio streams for notifications"
import android.media.ToneGenerator; //Used to provide the alert tone
import edu.luc.etl.cs313.android.simplestopwatch.R;

class RunningState implements StopwatchState {

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionInc(); //Continues to increment the runtime (just like the stoppedstate)
        sm.actionResetPause(); //Resets pause time
        if (sm.actionGetRunTime() == 99){
            final ToneGenerator alert = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
            alert.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            sm.toCountDownState();
            // If the runtime reaches the two digit max, it goes to countdownstate
        } //Which will then take the app to the CountDownState

    }

    /*@Override
    public void onLapReset() {
        sm.actionLap();
        sm.toLapRunningState();
    }*/

    @Override
    public void onTick() { //Changed from incrementing to count until three seconds to go to onPause()
        sm.actionIncPauseTime();
        if (sm.actionGetPauseTime() == 3){
            final ToneGenerator alert = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
            alert.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            sm.toCountDownState();
            //If the pause reaches three seconds, it goes to the onPause function
        } //Which takes the app to the CountDownState
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
