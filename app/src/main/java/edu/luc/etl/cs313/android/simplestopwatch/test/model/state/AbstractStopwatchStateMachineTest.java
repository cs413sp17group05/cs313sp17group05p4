package edu.luc.etl.cs313.android.simplestopwatch.test.model.state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnTickListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.state.StopwatchStateMachine;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * Testcase superclass for the stopwatch state machine model. Unit-tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author laufer
 * @see ://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractStopwatchStateMachineTest {

    private StopwatchStateMachine model;

    private UnifiedMockDependency dependency;

    @Before
    public void setUp() throws Exception {
        dependency = new UnifiedMockDependency();
    }

    @After
    public void tearDown() {
        dependency = null;
    }

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final StopwatchStateMachine model) {
        this.model = model;
        if (model == null)
            return;
        this.model.setUIUpdateListener(dependency);
        this.model.actionInit();
    }

    protected UnifiedMockDependency getDependency() {
        return dependency;
    }

    /**
     * Verifies that we're initially in the stopped state (and told the listener
     * about it).
     */
    @Test
    public void testPreconditions() {
        assertEquals(R.string.STOPPED, dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press start, press button 5 times, time displayed is 6 seconds.
     *
     */
    @Test
    public void testScenarioRun() {
        assertTimeEquals(0);
        // directly invoke the button press event handler methods
        model.onStartStop(); //this automatically adds one second
        buttonPress(5); //pressing five more times will make the time 6 seconds
        assertTimeEquals(6);
    }

    /*
    Verifies that time is 0 originally and that pressing the button puts machine into a running state.
     */
    @Test
    public void testRunningState() {
        assertTimeEquals(0);
        model.onStartStop();
        assertEquals(R.string.RUNNING, dependency.getState());
    }

    /*
    Verifies that putting the machine into a running state and then pausing for 3 seconds sets the machine
    to a countdown state.
     */
    @Test
    public void testRunningToCountdown()
    {
        assertTimeEquals(0);
        model.onStartStop();
        buttonPress(5);
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onTick(); //each onTick is one second passed. The three are the 3 second pause time.
        model.onTick();
        model.onTick();
        assertEquals(R.string.COUNTDOWN, dependency.getState());

    }
    /*
    Verifies that when machine is in a countdown state, pressing the button will cause it to reset
    and be in a stopped state with a time of 0.
     */
    @Test
    public void testRunningToReset()
    {
        assertTimeEquals(0);
        model.onStartStop();
        buttonPress(5);
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onTick();
        model.onTick();
        model.onTick();
        assertEquals(R.string.COUNTDOWN, dependency.getState());
        model.onStartStop();
        assertTimeEquals(0);
        assertEquals(R.string.STOPPED,dependency.getState());
    }
    /*
    Verifies scenario of pressing button 6 times, time equals 6, pause time 3 seconds into countdown state,
    counts down for 3 tick events, verifies time is 3 seconds.
     */
    @Test
    public void testCountdownState()
    {
        assertTimeEquals(0);
        model.onStartStop();
        buttonPress(5);
        assertTimeEquals(6);
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onTick();
        model.onTick();
        model.onTick();
        assertEquals(R.string.COUNTDOWN, dependency.getState());
        model.onTick();
        model.onTick();
        model.onTick();
        assertTimeEquals(3);
    }
    /*
    Verifies scenario where machine goes to running state, goes to countdown state, time equals 0 and enters alarming state.
     */
    @Test
    public void testAlarmingState()
    {
        assertTimeEquals(0);
        model.onStartStop();
        buttonPress(1);
        assertEquals(R.string.RUNNING, dependency.getState());
        model.onTick();
        model.onTick();
        model.onTick();
        assertEquals(R.string.COUNTDOWN, dependency.getState());
        model.onTick();
        model.onTick();
        assertTimeEquals(0);
        assertEquals(R.string.ALARMING, dependency.getState());
    }

    /**
     * Sends the given number of tick events to the model.
     *
     *  @param n the number of tick events
     */
    protected void buttonPress(final int n) {
        for (int i = 0; i < n; i++)

            model.actionInc();
    }
    protected void onPauseRepeat(final int n) {
        for (int i = 0; i < n; i++)

            model.actionIncPauseTime();
    }
    /**
     * Checks whether the model has invoked the expected time-keeping
     * methods on the mock object.
     */
    protected void assertTimeEquals(final int t) {
        assertEquals(t, dependency.getTime());
    }
}

/**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author laufer
 */
 class UnifiedMockDependency implements TimeModel, ClockModel, StopwatchUIUpdateListener {

    private int timeValue = -1, stateId = -1;

    private int runningTime = 0;

    private int pauseTime = 0;

    private boolean started = false;

    public int getTime() {
        return timeValue;
    }

    public int getState() {
        return stateId;
    }

    public boolean isStarted() {
        return started;
    }

    @Override
    public void updateTime(final int timeValue) {
        this.timeValue = timeValue;
    }

    @Override
    public void updateState(final int stateId) {
        this.stateId = stateId;
    }

    @Override
    public void setOnTickListener(OnTickListener listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void start() {
        started = true;
    }

    @Override
    public void stop() {
        started = false;
    }

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        runningTime++;
    }

    @Override
    public void decRuntime() {
        runningTime--;
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void resetPausetime() {
    runningTime=0;
    }

    @Override
    public void incPausetime() {
     pauseTime++;
    }

    @Override
    public int getPausetime() {
        return pauseTime;
    }

    public void resetTime(){
        return;
    }
    public int getUI() {
        return 0;
    }

}