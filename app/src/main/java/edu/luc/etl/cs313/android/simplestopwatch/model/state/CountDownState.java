package edu.luc.etl.cs313.android.simplestopwatch.model.state;
//import org.junit.rules.Stopwatch;
import android.media.AudioManager;//used for Stream_Notification which is "used to identify the volume of audio streams for notifications"
import android.media.ToneGenerator; //Used to provide the alert tone
import edu.luc.etl.cs313.android.simplestopwatch.R;

/**
 * Created by iband on 4/7/2017.
 */

class CountDownState implements StopwatchState {
    public CountDownState(final StopwatchSMStateView sm){this.sm = sm;}
    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop(){
        sm.actionStop(); //Stops the time
        sm.actionInit(); //Resets everything (stopped state and reset runtime)
    }

    @Override
    public void onTick(){
        sm.actionDec(); //Decrement runtime
        if (sm.actionGetRunTime() == 0){ //Once runtime reaches 0, go to alarming state
            final ToneGenerator alert = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
            alert.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT);
            sm.toAlarmedState();
        }
    }

    @Override
    public void updateView(){
        sm.updateUIRuntime();
    }

    @Override
    public int getId(){
        return R.string.COUNTDOWN;
    }
}
