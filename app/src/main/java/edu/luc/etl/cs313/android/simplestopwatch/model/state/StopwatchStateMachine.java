package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateSource;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIListener;
//import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnPauseListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnTickListener;

/**
 * The state machine for the state-based dynamic model of the stopwatch.
 * This interface is part of the State pattern.
 *
 * @author laufer
 */
public interface StopwatchStateMachine extends StopwatchUIListener, OnTickListener, StopwatchUIUpdateSource, StopwatchSMStateView{ }

/*
This interface extends all of the different interfaces that contain methods that pertain to state machine actions or state changes
(OnTickListener and StopwatchUIListener for example that pass actions and states along).
 */