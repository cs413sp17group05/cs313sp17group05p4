package edu.luc.etl.cs313.android.simplestopwatch.common;

/**
 * A source of UI update events for the stopwatch.
 * This interface is typically implemented by the model.
 *
 * @author laufer
 */

/*
Listens to the actions and state changes from the onStartStop() method and then is implemented in the stopwatch facades to
basically bridge the backend to the front end of this app.
 */

public interface StopwatchUIUpdateSource {
    void setUIUpdateListener(StopwatchUIUpdateListener listener);
}
